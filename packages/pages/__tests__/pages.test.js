'use strict';

const pages = require('..');
const assert = require('assert').strict;

assert.strictEqual(pages(), 'Hello from pages');
console.info('pages tests passed');
